package com.doglong.skillsystem.skill

import bukkitkotlin.utils.StringUtils.color
import com.doglong.skillsystem.SkillSystemApi
import com.doglong.skillsystem.condition.Condition
import com.doglong.skillsystem.utils.VirusConfig
import org.bukkit.configuration.ConfigurationSection
import java.util.*

class Skill(val instanceKey: String, override val config: ConfigurationSection): VirusConfig {

    val name: String = config.getString("properties.name", instanceKey).color()

    val action: Action? = config.getConfigurationSection("skill")?.let { Action(it) }

    val upgrade: LinkedList<MutableList<Condition>> = SkillSystemApi.conditionManager.loadConditions(config.getConfigurationSection("properties.upgrade"))

    val condition: MutableList<Condition> = SkillSystemApi.conditionManager.loadConditions(config.getMapList("properties.upgrade"))

    val demotion: LinkedList<MutableList<Condition>> = SkillSystemApi.conditionManager.loadConditions(config.getConfigurationSection("properties.demotion"))

    val description: MutableList<String> = config.getStringList("description").apply { replaceAll{it.color()} }

    class Action(section: ConfigurationSection) {

        val trigger: String? = section.getString("trigger")

        val conditions: LinkedList<MutableList<Condition>> = SkillSystemApi.conditionManager.loadConditions(section.getConfigurationSection("condition"))
    }
}