package com.doglong.skillsystem.common

import bukkitkotlin.lang.LocalizeConfig


class Data<T>(val success: Boolean) {

    companion object {

        fun <T> success(data: T? = null): Data<T> {
            val instance = Data<T>(true)
            instance.data = data
            return instance
        }

        fun <T> failure(message: String? = null): Data<T> {
            val data = Data<T>(false)
            data.message = message
            return data
        }

        fun <T> failure(data: Data<*>): Data<T> {
            return failure(data.message)
        }

        fun <T> failureLang(lang: String, vararg params: Any?): Data<T> {
           return failure(LocalizeConfig.parseString(lang, *params))
        }
    }

    var data: T? = null

    var message: String? = null

    val failure: Boolean
        get() = !success
}