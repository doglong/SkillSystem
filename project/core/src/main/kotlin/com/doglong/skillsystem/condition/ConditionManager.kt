package com.doglong.skillsystem.condition

import com.doglong.skillsystem.common.Data
import org.bukkit.configuration.ConfigurationSection
import org.bukkit.entity.Player
import java.util.*

interface ConditionManager {

    fun loadConditions(section: ConfigurationSection?): LinkedList<MutableList<Condition>>

    fun loadConditions(list: MutableList<Map<*, *>>): MutableList<Condition>

    fun executorConditions(
        player: Player,
        conditions: LinkedList<MutableList<Condition>>, level: Int,
        config: ConfigurationSection? = null
    ): Data<Runnable>

    fun executorConditions(
        player: Player,
        conditions: MutableList<Condition>,
        config: ConfigurationSection? = null
    ): Data<Runnable>
}