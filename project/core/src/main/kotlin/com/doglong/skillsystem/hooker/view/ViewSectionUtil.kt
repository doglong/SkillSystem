package com.doglong.skillsystem.hooker.view

import bukkitkotlin.plugin.plugin
import org.bukkit.configuration.ConfigurationSection
import org.bukkit.configuration.MemorySection
import java.util.*
import kotlin.collections.LinkedHashMap

@Suppress("UNCHECKED_CAST")
object ViewSectionUtil {

    internal val Any?.asMap: MutableMap<String, Any?>?
        get() = if (this is MutableMap<*, *>) this as MutableMap<String, Any?> else null

    internal fun checkInput(k: String, map: MutableMap<String, Any?>) {
        if (k.endsWith("_textbox") || map["type"].toString() == "textbox") {
            val actionsMap = map.getMap("actions")
            actionsMap["textChange"] = "方法.发包('${plugin.name}_TextChange','${k}',方法.取组件值('${k}','text'));${ actionsMap["textChange"]?.toString() ?: "" }"
            actionsMap["click"] = "方法.设置组件值('${k}', 'text', '');${ actionsMap["click"]?.toString() ?: "" }"
        }
    }

    internal fun MutableMap<String, Any?>.getMap(key: String): MutableMap<String, Any?> {
        return this.computeIfAbsent(key) { mutableMapOf<String, Any?>() } as MutableMap<String, Any?>
    }

    internal fun MutableMap<String, Any?>.check() {
        for ((k, v) in this) {
            v.asMap?.let { checkInput(k, it) }
        }
    }

    internal fun MutableMap<String, Any?>.deepSet(key: String, value: Any?) {
        if (key.contains(".")) {
            val split = key.split(".")
            var map = this
            for (i in 0 until split.size - 1) {
                map = map.getMap(split[i])
            }
            map[split[split.size - 1]] = value
        }else {
            this[key] = value
        }
    }

    internal fun MutableMap<String, Any?>.deepGet(key: String): Any? {
        if (key.contains(".")) {
            val split = key.split(".")
            var map = this
            for (i in 0 until split.size - 1) {
                map[split[i]]?.let {
                    map = it as MutableMap<String, Any?>
                } ?: return null
            }
            return map[split[split.size - 1]]
        }
        return this[key]
    }

    internal fun MutableMap<String, Any?>.copy(collections: MutableMap<String, Any?>, replace: (String.() -> String)? = null): MutableMap<String, Any?> = handlerEReplace(this, collections, replace)

    internal fun ConfigurationSection.asValues(): MutableMap<String, Any?> {
        val map = LinkedHashMap<String, Any?>()
        for ((k, v) in this.getValues(false)) {
            map[k] = if (v is MemorySection) { v.asValues() }else v
        }
        return map
    }

    private fun handlerEReplace(originals: MutableMap<String, Any?>, collections: MutableMap<String, Any?>, replace: (String.() -> String)? = null): MutableMap<String, Any?> {
        return if (replace != null) {
            var key: String
            var value: Any?
            originals.entries.forEach {
                key = it.key
                value = it.value
                when (value) {
                    is String -> {
                        value = replace.invoke(value as String)
                    }
                    is List<*> -> {
                        val newList = LinkedList<String>()
                        (value as List<*>).forEach { text ->
                            newList.add(replace.invoke(text.toString()))
                        }
                        value = newList
                    }
                    is MutableMap<*, *> -> {
                        value = handlerEReplace(value as MutableMap<String, Any?>, LinkedHashMap(), replace)
                    }
                }
                collections[key] = value
            }
            collections
        }else {
            collections.putAll(originals)
            collections
        }
    }

}