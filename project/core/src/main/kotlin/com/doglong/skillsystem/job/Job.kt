package com.doglong.skillsystem.job

import bukkitkotlin.logging.info
import bukkitkotlin.utils.StringUtils.color
import com.doglong.skillsystem.SkillSystemApi
import com.doglong.skillsystem.skill.Skill
import com.doglong.skillsystem.utils.VirusConfig
import org.bukkit.configuration.ConfigurationSection

class Job(val instanceKey: String, override val config: ConfigurationSection): VirusConfig {

    val name: String = config.getString("properties.name", instanceKey).color()

    val manaName: String = config.getString("properties.mana-name", instanceKey).color()

    val exp: String get() = config.getString("properties.exp")

    val maxLevel: String get() = config.getString("properties.max-level")

    val actionByUpgrade: String get() = config.getString("properties.upgrade")

    val joinConditions = SkillSystemApi.conditionManager.loadConditions(config.getMapList("properties.condition"))

    val attribute: MutableMap<String, String?> = mutableMapOf<String, String?>().apply {
        config.getConfigurationSection("attributes")?.let { section ->
            for (key in section.getKeys(false)) {
                this[key] = section.getString(key)
            }
        }
    }

    val skillList: MutableList<Skill> = mutableListOf<Skill>().apply {
        for (instanceKey in config.getStringList("skills")) {
            val skillSection = SkillSystemApi.skillManager.getSkill(instanceKey)
            if (skillSection == null) {
                info("&6职业 '&f{0}&6' 配置中的技能列表中的 '&f{1}&6' 技能不存在.", this@Job.instanceKey, instanceKey)
            }else {
                this += skillSection
            }
        }
    }

}