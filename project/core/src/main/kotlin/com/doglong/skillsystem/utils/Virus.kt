package com.doglong.skillsystem.utils

import com.doglong.skillsystem.SkillSystemApi
import com.doglong.skillsystem.profile.PlayerProfile
import com.doglong.skillsystem.profile.SkillProfile
import org.bukkit.entity.Player
import shark.virus.common.VirusVariable

object Virus {

    fun PlayerProfile.vars(): VirusVariable {
        val vars = virus.newVarTable()
        val job = jobProfile
        job?.let { vars.setConfig(it.job.config) }
        vars.setVal("level", job?.level ?: 0)
        vars.setVal("job_points", job?.points ?: 0)
        vars.setVal("exp", job?.exp ?: 0)
        vars.setVal("max_exp", job?.maxExp ?: 0)
        vars.setVal("mana", mana)
        vars.setVal("max_mana", SkillSystemApi.attributeHooker.getMana(player))
        vars.setVal("skill_size", allSkills.size)
        vars.setVal("points", points)
        vars.setVal("job_points", job?.points ?: 0)
        return vars
    }

    fun PlayerProfile.replaceContents(): String.() -> String = { this.inlineScript(player, vars()) }

    fun SkillProfile?.vars(): VirusVariable {
        val vars = virus.newVarTable()
        this?.let { profile ->
            vars.setConfig(profile.skill.config)
            vars.setVal("id", profile.instanceKey)
            vars.setVal("level", profile.level)
            vars.setVal("cooldown", profile.cooldown)
            vars.setVal("key", profile.shortcutKey)
            vars.setVal("can_bind", true)
        }
        return vars
    }

    fun SkillProfile?.replaceContents(player: Player, replace: String.() -> String = {this}): String.() -> String = {
        val vars = vars()
        replace.invoke(this).inlineScript(player, vars)
    }
}