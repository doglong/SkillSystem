package com.doglong.skillsystem.common

enum class OperateType {
    ADD, TAKE, SET
}