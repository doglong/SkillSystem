package com.doglong.skillsystem.database

import com.doglong.skillsystem.job.Job
import com.doglong.skillsystem.profile.JobProfile
import com.doglong.skillsystem.profile.PlayerProfile
import org.bukkit.entity.Player

interface IDatabase {

    /**
     * 更新数据
     */
    fun update(profile: PlayerProfile, callback: Boolean.() -> Unit = {})

    /**
     * 更新数据
     */
    fun update(profile: JobProfile, callback: Boolean.() -> Unit = {})

    /**
     * 查询数据
     */
    fun select(player: Player): PlayerProfile

    /**
     * 查询数据
     */
    fun select(player: Player, instance: Job): JobProfile?

    /**
     * 删除数据
     */
    fun delete(player: Player, callback: Boolean.() -> Unit = {})
}