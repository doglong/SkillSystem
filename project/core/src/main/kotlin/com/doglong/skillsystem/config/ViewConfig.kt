package com.doglong.skillsystem.config

import bukkitkotlin.Awake
import bukkitkotlin.Config
import bukkitkotlin.LifeCycle
import com.doglong.skillsystem.SkillSystemApi
import org.bukkit.configuration.file.FileConfiguration

object ViewConfig {

    @Config("guis/skill.yml")
    lateinit var guiConfig: FileConfiguration

    @Config("guis/bar.yml")
    lateinit var barConfig: FileConfiguration

    @Awake(lifeCycle = LifeCycle.RELOAD)
    private fun reload() {
        SkillSystemApi.viewHooker.loadGui(guiConfig)
        SkillSystemApi.viewHooker.loadBar(barConfig)
    }
}