package com.doglong.skillsystem.profile

import com.doglong.skillsystem.SkillSystemApi
import com.doglong.skillsystem.skill.Skill

data class SkillProfile(
    val instanceKey: String,
    val exclusive: Boolean
) {

    /**
     * 技能实例
     */
    val skill: Skill
        get() = SkillSystemApi.skillManager.getSkill(this.instanceKey) ?: throw NullPointerException("")

    /**
     * 技能等级
     */
    var level: Int = 0

    /**
     * 技能绑定快捷键
     */
    var shortcutKey: String? = null

    /**
     * 技能的几率结束时间
     */
    var recordTime: Long = 0

    /**
     * 技能的冷却时间毫秒
     */
    var recordCooldown: Long = 0

    /**
     * 技能的当前冷却时间的秒数
     */
    val cooldown: Int
        get() {
            val time = System.currentTimeMillis()
            return if (recordTime > time) ((recordTime - time) / 1000).toInt() + 1  else 0
        }
}