package com.doglong.skillsystem.hooker.view.component

import org.bukkit.configuration.file.FileConfiguration

open class ViewContainerPart(override val indexName: String): ViewPart {

    val components = LinkedHashMap<String, ViewPart>()

    fun add(stream: ViewPart) {
        this.components[stream.indexName] = stream
    }

    fun remove(indexName: String) {
        this.components.remove(indexName)
    }

    fun clear() { this.components.clear() }

    operator fun get(indexName: String): ViewPart? {
        for ((key, value) in this.components) {
            if (key == indexName) {
                return value
            }
            if (value is ViewContainerPart) {
                value[indexName]?.let {
                    return it
                }
            }
        }
        return null
    }

    @Suppress("UNCHECKED_CAST")
    fun <T: ViewPart> getOrDefault(indexName: String, def: T): T {
        return this[indexName]?.let { it as T } ?: def
    }

    @Suppress("UNCHECKED_CAST")
    fun <T: ViewPart> getOrAdd(indexName: String, def: T): T {
        val stream = this[indexName]
        if (stream == null) {
            this.add(def)
            return def
        }
        return stream as T
    }

    override fun write(config: FileConfiguration) {
        for ((_, stream) in components) {
            stream.write(config)
        }
    }
}