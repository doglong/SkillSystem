package com.doglong.skillsystem.hooker.view.component

import com.doglong.skillsystem.hooker.view.ViewSectionUtil
import com.doglong.skillsystem.hooker.view.ViewSectionUtil.copy
import com.doglong.skillsystem.hooker.view.ViewSectionUtil.deepGet
import com.doglong.skillsystem.hooker.view.ViewSectionUtil.deepSet
import org.bukkit.configuration.file.FileConfiguration

@Suppress("UNCHECKED_CAST")
open class ViewMapPart(final override val indexName: String, values: MutableMap<String, Any?>): ViewPart {

    private val values: MutableMap<String, Any?> = values.copy(LinkedHashMap()) { this.replace("<index_name>", indexName) }

    init {
        ViewSectionUtil.checkInput(indexName, this.values)
    }

    override fun write(config: FileConfiguration) {
        config.set(indexName, values)
    }

    operator fun set(key: String, value: Any?) {
        this.values.deepSet(key, value)
    }

    operator fun get(key: String): Any? {
        return this.values.deepGet(key)
    }

    @Suppress("UNCHECKED_CAST")
    fun <T> getOrDefault(key: String, def: T): T {
        return this[key]?.let { it as T } ?: def
    }

}