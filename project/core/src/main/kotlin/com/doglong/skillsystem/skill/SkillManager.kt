package com.doglong.skillsystem.skill

import com.doglong.skillsystem.common.Data
import com.doglong.skillsystem.profile.PlayerProfile
import com.doglong.skillsystem.profile.SkillProfile
import org.bukkit.entity.Player
import shark.virus.common.VirusVariable

interface SkillManager {

    /**
     * 全部技能的索引名
     */
    val keySet: Set<String>

    /**
     * 全部技能的集合
     */
    val skills: Collection<Map.Entry<String, Skill>>

    /**
     * 公共技能列表
     */
    val publicSkills: MutableList<Skill>

    /**
     * 释放一个技能
     */
    fun cast(player: Player, profile: PlayerProfile, skill: Skill, vars: VirusVariable.() -> Unit = {})

    /**
     * 设置技能的快捷键
     */
    fun setSkillShortcutKey(profile: PlayerProfile, skill: Skill, shortcutKey: String?) {
        profile.getSkill(skill)?.let { setSkillShortcutKey(profile, it, shortcutKey) }
    }

    /**
     * 设置技能的快捷键
     */
    fun setSkillShortcutKey(profile: PlayerProfile, skillProfile: SkillProfile, shortcutKey: String?)

    /**
     * 升级技能
     */
    fun upgradeSkill(profile: PlayerProfile, skillProfile: SkillProfile): Data<Any>

    /**
     * 降级技能
     */
    fun demotionSkill(profile: PlayerProfile, skillProfile: SkillProfile): Data<Any>

    /**
     * 添加专属技能
     */
    fun addExclusiveSkill(profile: PlayerProfile, skill: Skill): Data<Any>

    /**
     * 移除专属技能
     */
    fun removeExclusiveSkill(profile: PlayerProfile, skill: Skill): Data<Any>

    /**
     * 根据技能的索引名获取技能实例
     */
    fun getSkill(instanceKey: String): Skill?
}