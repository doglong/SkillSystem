package com.doglong.skillsystem.utils

import bukkitkotlin.lang.LocalizeConfig.sendLang
import bukkitkotlin.utils.Numbers.asInt
import bukkitkotlin.utils.StringUtils.color
import com.doglong.skillsystem.SkillSystemApi
import com.doglong.skillsystem.SkillSystemApi.Companion.skillSystemIsLoadedProfile
import com.doglong.skillsystem.SkillSystemApi.Companion.skillSystemProfile
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack
import shark.virus.common.VirusFunction
import shark.virus.common.customFunction

class VirusExtension {

    @VirusFunction("取技能等级")
    val getSkillLevel = customFunction<CommandSender>(2) { _, _, arguments ->
        val sender = arguments[0]
        return@customFunction  if (sender is Player && sender.skillSystemIsLoadedProfile) {
            sender.skillSystemProfile.getSkill(arguments[1].toString())?.level ?: 0
        }else 0
    }

    @VirusFunction("取职业等级")
    val getJobLevel = customFunction<CommandSender>(1) { _, _, arguments ->
        val sender = arguments[0]
        return@customFunction  if (sender is Player && sender.skillSystemIsLoadedProfile) {
            val profile = sender.skillSystemProfile
            if (arguments.size > 1) {
                val job = SkillSystemApi.jobManager.getJob(arguments[1].toString())
                if (job == null) {
                    sender.sendLang("NO_JOB")
                    return@customFunction 0
                }
                SkillSystemApi.database.select(sender, job)?.level ?: 0
            }else {
                profile.jobProfile?.level
            }
        }else 0
    }

    @VirusFunction("设置玩家数据")
    val setTag = customFunction<CommandSender>(3) { _, _, arguments ->
        val sender = arguments[0]
        if (sender is Player && sender.skillSystemIsLoadedProfile) {
            val profile = sender.skillSystemProfile
            profile.tags[arguments[1].toString()] = arguments[2].toString()
            profile.push()
        }
    }

    @VirusFunction("取玩家数据")
    val getTag = customFunction<CommandSender>(2) { _, _, arguments ->
        val sender = arguments[0]
        if (sender is Player && sender.skillSystemIsLoadedProfile) {
            val profile = sender.skillSystemProfile
            return@customFunction profile.tags[arguments[1].toString()]
        }
        return@customFunction null
    }

    @VirusFunction("删除玩家数据")
    val removeTag = customFunction<CommandSender>(2) { _, _, arguments ->
        val sender = arguments[0]
        if (sender is Player && sender.skillSystemIsLoadedProfile) {
            val profile = sender.skillSystemProfile
            profile.tags.remove(arguments[1].toString())
        }
    }

    @VirusFunction("取背包物品数量")
    val getInventoryItemAmount = customFunction<CommandSender>(2) { _, _, arguments ->
        val sender = arguments[0]
        if (sender is Player) {
            val inventory = sender.inventory
            val itemMatch = arguments[1].toItemMatch() ?: return@customFunction 0
            var amount = 0
            for (i in 0 until 36) {
                val stack = inventory.getItem(i)
                if (stack != null && itemMatch.isItem(stack)) {
                    amount += stack.amount
                }
            }
            return@customFunction amount
        }
        return@customFunction 0
    }

    @VirusFunction("扣除背包物品数量")
    val takeInventoryItemAmount = customFunction<CommandSender>(3) { _, _, arguments ->
        val sender = arguments[0]
        if (sender is Player) {
            val inventory = sender.inventory
            val itemMatch = arguments[1].toItemMatch() ?: return@customFunction false
            var amount = arguments[2].asInt()
            for (i in 0 until 36) {
                val stack = inventory.getItem(i)
                if (stack != null && itemMatch.isItem(stack)) {
                    val a = stack.amount
                    if ( amount > a ) {
                        amount -= a
                        inventory.setItem(i, null)
                    }else {
                        stack.amount -= amount
                        amount = 0
                        break
                    }
                }
            }
            return@customFunction amount == 0
        }
        return@customFunction false
    }

    private fun Any?.toItemMatch(): ItemMatch? {
        if (this != null) {
            val str = this.toString().color()
            var id: String? = null
            var name: String? = null
            var lore: String? = null
            if (str.contains(";")) {
                for (line in str.split(";")) {
                    val indexOf = line.indexOf("=")
                    if (indexOf > 0) {
                        val type = line.substring(0, indexOf)
                        val value = line.substring(indexOf + 1)
                        when(type) {
                            "id" -> id = value
                            "name" -> name = value
                            else -> lore = value
                        }
                    }
                }
            }else if (str.contains("=")){
                val indexOf = str.indexOf("=")
                val type = str.substring(0, indexOf)
                val value = str.substring(indexOf + 1)
                when(type) {
                    "id" -> id = value
                    "name" -> name = value
                    else -> lore = value
                }
            }
            return ItemMatch(id, name?.color(), lore?.color())
        }
        return null
    }

    // 'id=123;name=123;lore=123'
    data class ItemMatch(val id: String?, val name: String?, val lore: String?) {

        fun isItem(itemStack: ItemStack): Boolean {
            if (id != null && itemStack.type.name != id.uppercase()) {
                return false
            }
            val meta = itemStack.itemMeta
            if (meta == null && (name != null || lore != null)) {
                return false
            }
            if (name != null && name != meta.displayName) {
                return false
            }
            return !(lore != null && (!meta.hasLore() || !meta.lore.contains(lore)))
        }

    }
}