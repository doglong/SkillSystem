package com.doglong.skillsystem.condition

class Condition(map: Map<*, *>) {

    val `if`: String? = map["if"]?.toString()
    val failure: String? = map["failure"]?.toString()
    val success: String? = map["success"]?.toString()

}