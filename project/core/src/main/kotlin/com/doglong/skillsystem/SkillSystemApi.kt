package com.doglong.skillsystem

import com.doglong.skillsystem.condition.ConditionManager
import com.doglong.skillsystem.database.IDatabase
import com.doglong.skillsystem.hooker.AttributeHooker
import com.doglong.skillsystem.hooker.ViewHooker
import com.doglong.skillsystem.job.JobManager
import com.doglong.skillsystem.profile.PlayerProfile
import com.doglong.skillsystem.skill.SkillManager
import org.bukkit.entity.Player
import java.util.concurrent.ConcurrentHashMap

class SkillSystemApi {

    companion object {

        private lateinit var instance: SkillSystemApi

        val database: IDatabase get() = instance.database
        val skillManager: SkillManager get() = instance.skillManager
        val jobManager: JobManager get() = instance.jobManager
        val conditionManager: ConditionManager get() = instance.conditionManager
        val viewHooker: ViewHooker get() = instance.viewHooker
        val attributeHooker: AttributeHooker get() = instance.attributeHooker

        /**
         * 根据玩家获取玩家数据
         */
        val Player.skillSystemProfile: PlayerProfile get() = instance.profiles[name]!!

        /**
         * 判断这个玩家是否已经加载玩家数据
         */
        val Player.skillSystemIsLoadedProfile: Boolean get() = instance.profiles.containsKey(name)
    }

    init {
        instance = this
    }

    lateinit var skillManager: SkillManager
    lateinit var jobManager: JobManager
    lateinit var database: IDatabase
    lateinit var conditionManager: ConditionManager
    var viewHooker: ViewHooker = ViewHooker.Default
    var attributeHooker: AttributeHooker = AttributeHooker.Default
    val profiles = ConcurrentHashMap<String, PlayerProfile>()
}