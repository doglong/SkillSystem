package com.doglong.skillsystem.config

import bukkitkotlin.Awake
import bukkitkotlin.Config
import bukkitkotlin.LifeCycle
import bukkitkotlin.Priority
import bukkitkotlin.functions.submit
import bukkitkotlin.logging.debugLevel
import com.doglong.skillsystem.SkillSystemApi
import com.doglong.skillsystem.SkillSystemApi.Companion.skillSystemIsLoadedProfile
import com.doglong.skillsystem.SkillSystemApi.Companion.skillSystemProfile
import org.bukkit.Bukkit
import org.bukkit.configuration.ConfigurationSection
import org.bukkit.scheduler.BukkitTask

object RootConfig {

    val ENABLE_SEPARATE_POINTS: Boolean
        get() = config.getBoolean("options.separate-points")

    val ENABLE_SYNC_LEVEL: Boolean
        get() = config.getBoolean("options.level.sync")

    val SYNC_LEVEL_PAPI: String?
        get() = config.getString("options.level.papi")

    val ENABLE_LOCK_KEY: Boolean
        get() = config.getBoolean("options.lock-key")

    val SHORTCUT_KEY_LIST: MutableList<String>
        get() = config.getStringList("options.keys")

    val EXCLUSIVE_SKILL_AMOUNT: Int
        get() = config.getInt("options.custom-amount")

    val KEY_INTERACT_FREQUENCY: Int
        get() = config.getInt("options.key-interact-frequency", 100)

    val CAST_WORLD_BLACK_LIST: MutableList<String>
        get() = config.getStringList("options.cast-blacklist")

    @Config("config.yml")
    lateinit var config: ConfigurationSection

    private var runnable: BukkitTask? = null

    @Awake(lifeCycle = LifeCycle.RELOAD, priority = Priority.MONITOR)
    private fun enable() {

        debugLevel = config.getInt("debug-level")
        submit(1, async = true) {
            SHORTCUT_KEY_LIST.forEach{ SkillSystemApi.viewHooker.unregisterKey(it) }
            runnable?.cancel()
            val period = config.getLong("options.restore.period", 100)
            val amount = config.getInt("options.restore.mana", 3)
            runnable = submit(period, period) {
                Bukkit.getOnlinePlayers().filter { it.skillSystemIsLoadedProfile }.forEach {
                    val profile = it.skillSystemProfile
                    val maxMana = SkillSystemApi.attributeHooker.getMana(it)
                    if (profile.mana < maxMana) {
                        val value = profile.mana + amount + SkillSystemApi.attributeHooker.getManaRestore(it.player)
                        profile.mana = value.coerceAtMost(maxMana).coerceAtLeast(0)
                    }
                }
            }
            SHORTCUT_KEY_LIST.forEach{ SkillSystemApi.viewHooker.registerKey(it) }
        }
    }

}