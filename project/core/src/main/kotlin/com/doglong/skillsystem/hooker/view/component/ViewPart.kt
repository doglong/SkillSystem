package com.doglong.skillsystem.hooker.view.component

import org.bukkit.configuration.file.FileConfiguration

interface ViewPart {

    val indexName: String

    fun write(config: FileConfiguration)

}