package com.doglong.skillsystem.profile

import bukkitkotlin.utils.Numbers.asInt
import com.doglong.skillsystem.SkillSystemApi
import com.doglong.skillsystem.job.Job
import com.doglong.skillsystem.skill.Skill
import com.doglong.skillsystem.utils.runScript
import org.bukkit.Bukkit
import org.bukkit.entity.Player
import java.util.*

class JobProfile(val uniqueId: UUID, val name: String, val instanceKey: String) {

    /**
     * 玩家实例
     */
    val player: Player get() = Bukkit.getPlayer(uniqueId)!!

    /**
     * 玩家是否在线
     */
    val isPlayerOnline: Boolean get() = Bukkit.getPlayer(uniqueId) != null

    /**
     * 职业实例
     */
    val job: Job
        get() = SkillSystemApi.jobManager.getJob(instanceKey)!!

    /**
     * 技能点数
     */
    var points: Int = 0

    /**
     * 当前等级的经验
     */
    var exp: Long = 0

    /**
     * 当前等级的最大经验
     */
    var maxExp: Long = 0

    /**
     * 当前等级
     */
    var level: Int = 0

    /**
     * 最大等级
     */
    val maxLevel: Int get() = this.job.runScript(this.player, "properties.max-level").asInt()

    /**
     * 当前的总经验
     */
    var experience: Long = 0

    /**
     * 当前职业的技能列表
     */
    var skills = mutableListOf<SkillProfile>()

    /**
     * 数据锁
     */
    var isSaving: Boolean = false
        private set

    /**
     * 根据技能索引名获取技能数据
     */
    fun getSkill(key: String): SkillProfile? = this.skills.firstOrNull { it.instanceKey == key }

    /**
     * 根据技能实例获取技能数据
     */
    fun getSkill(skill: Skill): SkillProfile? = getSkill(skill.instanceKey)

    /**
     * 根据技能快捷键索引位获取技能数据
     */
    fun getSkillByShortcutKey(shortcutKey: String): SkillProfile? = this.skills.firstOrNull { it.shortcutKey == shortcutKey }

    /**
     * 检查并更新数据
     */
    fun push() {
        if (!this.isSaving) {
            this.isSaving = true
            SkillSystemApi.database.update(this) {
                this@JobProfile.isSaving = false
            }
        }
    }
}