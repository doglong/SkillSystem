package com.doglong.skillsystem.hooker

import org.bukkit.entity.Player

interface PlaceholderHooker {

    fun setPlaceholder(player: Player, str: String): String

    object Default: PlaceholderHooker {
        override fun setPlaceholder(player: Player, str: String): String = str
    }
}