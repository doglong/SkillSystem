package com.doglong.skillsystem.hooker.view

import org.bukkit.entity.Player
import com.doglong.skillsystem.hooker.view.component.ViewScreen

@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
annotation class PacketId(
    val identity: String
)

fun runPacket(indexName: String? = null, length: Int = 0, async: Boolean = false, executor: (player: Player, screen: ViewScreen, args: List<String>) -> Unit): ViewPacket {
    return ViewPacket(indexName, length, async, executor)
}

data class ViewPacket(val indexName: String?, val length: Int, val async: Boolean, val executor: (player: Player, screen: ViewScreen, args: List<String>) -> Unit)
