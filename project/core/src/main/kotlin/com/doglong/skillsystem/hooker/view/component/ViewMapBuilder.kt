package com.doglong.skillsystem.hooker.view.component

import com.doglong.skillsystem.hooker.view.ViewSectionUtil.asValues
import com.doglong.skillsystem.hooker.view.ViewSectionUtil.copy
import org.bukkit.configuration.ConfigurationSection

class ViewMapBuilder (val indexName: String, val section: ConfigurationSection) {

    private val values = section.asValues()

    fun add(container: ViewContainerPart, index: Int = -1, replace: (String.() -> String)? = null, callback: ViewMapPart.() -> Unit = {}) {
        val indexName = if (index != -1) "${index}-${indexName}" else indexName
        val component = ViewMapPart(
            indexName,
            this.values.copy(mutableMapOf()) {
                replace?.invoke(replace("<i>", index.toString())) ?:
                replace("<i>", index.toString())
            }
        )
        callback.invoke(component)
        container.add(component)
    }

}