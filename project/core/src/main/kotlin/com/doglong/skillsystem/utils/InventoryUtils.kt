package com.doglong.skillsystem.utils

import bukkitkotlin.utils.ItemUtils.checkAir
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.entity.Player
import org.bukkit.inventory.Inventory
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.EnchantmentStorageMeta
import java.lang.reflect.Method

object InventoryUtils {

    private val storageContents: Method by lazy {
        try {
            Inventory::class.java.getMethod("getStorageContents")
        } catch (e: Exception) {
            try {
                Inventory::class.java.getMethod("getContents")
            } catch (e: Exception) {
                throw RuntimeException(e)
            }
        }
    }

    fun getEmptySlotCount(player: Player): Int {
        val inventory = player.inventory
        var count = 0
        for (i in 0..35) {
            if (inventory.getItem(i).checkAir()) {
                count ++
            }
        }
        return count
    }

    fun removeItem(inv: Inventory, item: ItemStack, amount: Int) {
        var stackSize: Int
        var remains = amount
        while (remains > 0) {
            stackSize = remains.coerceAtMost(item.maxStackSize)
            item.amount = stackSize
            inv.removeItem(item)
            remains -= stackSize
        }
    }

    fun addItem(inv: Inventory, item: ItemStack, amount: Int): Int {
        val stack = item.clone()
        var stackSize: Int
        val space = countSpace(inv, stack)
        var remains = amount.coerceAtMost(space)
        while (remains > 0) {
            stackSize = remains.coerceAtMost(stack.maxStackSize)
            stack.amount = stackSize
            inv.addItem(stack)
            remains -= stackSize
        }
        return (amount - space).coerceAtLeast(0)
    }

    fun dropItem(location: Location, item: ItemStack, amount: Int) {
        val stack = item.clone()
        var stackSize: Int
        var remains = amount
        while (remains > 0) {
            stackSize = remains.coerceAtMost(stack.maxStackSize)
            stack.amount = stackSize
            location.world.dropItem(location, stack)
            remains -= stackSize
        }
    }

    @Suppress("UNCHECKED_CAST")
    fun countSpace(inv: Inventory?, item: ItemStack): Int {
        var space = 0
        return try {
            val contents = storageContents.invoke(inv) as Array<ItemStack?>
            for (iStack in contents) {
                if (iStack != null && iStack.type != Material.AIR) {
                    if (item.matches(iStack)) {
                        space += item.maxStackSize - iStack.amount
                    }
                } else {
                    space += item.maxStackSize
                }
            }
            space
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }

    fun countItems(inv: Inventory, match: ItemStack.() -> Boolean): Int {
        var items = 0
        val contents = inv.contents
        for (iStack in contents) {
            if (iStack != null && match.invoke(iStack)) {
                items += iStack.amount
            }
        }
        return items
    }

    fun firstPartial(contents: Array<ItemStack?>, item: ItemStack): Int {
        contents.forEachIndexed { index, itemStack ->
            if (itemStack.matches(item)) {
                return index
            }
        }
        return -1
    }

    fun firstEmpty(contents: Array<ItemStack?>): Int {
        contents.forEachIndexed { index, itemStack ->
            if (itemStack.checkAir()) {
                return index
            }
        }
        return -1
    }

    fun ItemStack?.matches(stack2: ItemStack?): Boolean {
        return if (this == stack2) {
            true
        } else if (this != null && stack2 != null) {
            if (this.type != stack2.type) {
                false
            } else if (this.durability != stack2.durability) {
                false
            } else if (this.enchantments != stack2.enchantments) {
                false
            } else {
                if (this.itemMeta.hasDisplayName() || stack2.itemMeta.hasDisplayName()) {
                    if (!this.itemMeta.hasDisplayName() || !stack2.itemMeta.hasDisplayName()) {
                        return false
                    }
                    if (this.itemMeta.displayName != stack2.itemMeta.displayName) {
                        return false
                    }
                }
                try {
                    Class.forName("org.bukkit.inventory.meta.EnchantmentStorageMeta")
                    val book1 = this.itemMeta is EnchantmentStorageMeta
                    val book2 = stack2.itemMeta is EnchantmentStorageMeta
                    if (book1 != book2) {
                        return false
                    }
                    if (book1) {
                        val ench1 = (this.itemMeta as EnchantmentStorageMeta).storedEnchants
                        val ench2 = (stack2.itemMeta as EnchantmentStorageMeta).storedEnchants
                        if (ench1 != ench2) {
                            return false
                        }
                    }
                } catch (ignored: ClassNotFoundException) {
                }
                true
            }
        } else {
            false
        }
    }

}