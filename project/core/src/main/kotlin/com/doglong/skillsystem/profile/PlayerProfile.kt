package com.doglong.skillsystem.profile

import com.doglong.skillsystem.SkillSystemApi
import com.doglong.skillsystem.skill.Skill
import org.bukkit.Bukkit
import org.bukkit.entity.Player
import java.util.*

data class PlayerProfile(val uniqueId: UUID, val name: String) {

    /**
     * 玩家实例
     */
    val player: Player get() = Bukkit.getPlayer(uniqueId)!!

    /**
     * 玩家是否在线
     */
    val isPlayerOnline: Boolean get() = Bukkit.getPlayer(uniqueId) != null

    /**
     * 蓝量
     */
    var mana: Int = 0

    /**
     * 技能点数
     */
    var points: Int = 0

    /**
     * 当前技能释放所需的时间记录
     */
    var castRecordTime: Long = 0

    /**
     * 当前技能释放当前冷却时间的秒数
     */
    val castCooldown: Int
        get() {
            val time = System.currentTimeMillis()
            return if (castRecordTime > time) ((castRecordTime - time) / 1000).toInt() + 1  else 0
        }

    /**
     * 当前的职业数据
     */
    var jobProfile: JobProfile? = null

    /**
     * 持久化数据容器
     */
    var tags = mutableMapOf<String, String>()

    /**
     * 当前的专属技能列表
     */
    var skills = mutableListOf<SkillProfile>()

    /**
     * 当前的全部技能列表，包括当前职业的技能列表
     */
    val allSkills: MutableList<SkillProfile> get() {
            val profiles = mutableListOf<SkillProfile>()
            profiles += this.skills
            this.jobProfile?.let {
                profiles += it.skills
            }
            return profiles
        }

    /**
     * 是否有技能冷却中
     */
    val isOnCooldown: Boolean get() {
        val time = System.currentTimeMillis()
        this.skills.forEach {
            if (it.recordTime + 1000 > time) {
                return true
            }
        }
        this.jobProfile?.skills?.forEach {
            if (it.recordTime + 1000 > time) {
                return true
            }
        }
        return false
    }

    /**
     * 数据锁
     */
    var isSaving: Boolean = false
        private set

    /**
     * 根据技能索引名获取技能数据
     */
    fun getSkill(key: String): SkillProfile? = this.skills.firstOrNull { it.instanceKey == key } ?: this.jobProfile?.getSkill(key)

    /**
     * 根据技能实例获取技能数据
     */
    fun getSkill(skill: Skill): SkillProfile? = getSkill(skill.instanceKey)

    /**
     * 根据技能快捷键索引位获取技能数据
     */
    fun getSkillByShortcutKey(shortcutKey: String): SkillProfile? = this.skills.firstOrNull { it.shortcutKey == shortcutKey } ?: this.jobProfile?.getSkillByShortcutKey(shortcutKey)

    /**
     * 检查并更新数据
     */
    fun push() {
        if (!this.isSaving) {
            this.isSaving = true
            SkillSystemApi.database.update(this) {
                this@PlayerProfile.isSaving = false
            }
        }
    }
}