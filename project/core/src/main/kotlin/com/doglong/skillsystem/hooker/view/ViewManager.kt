package com.doglong.skillsystem.hooker.view

import bukkitkotlin.functions.reflect.Reflect.Companion.setAccessible
import bukkitkotlin.logging.DebugLevel
import bukkitkotlin.logging.debug
import bukkitkotlin.logging.info
import bukkitkotlin.plugin.plugin
import bukkitkotlin.utils.EventUtils.register
import bukkitkotlin.utils.Numbers.asNumber
import bukkitkotlin.utils.StringUtils.color
import bukkitkotlin.utils.StringUtils.isNotEmpty
import com.doglong.skillsystem.event.ViewRunFunctionEvent
import com.doglong.skillsystem.event.ViewScreenCloseEvent
import com.doglong.skillsystem.event.ViewTextChangeEvent
import com.doglong.skillsystem.hooker.view.component.ViewAnyPart
import eos.moe.dragoncore.api.gui.event.CustomPacketEvent
import eos.moe.dragoncore.network.PacketSender
import org.bukkit.Bukkit
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerCommandPreprocessEvent
import org.bukkit.event.player.PlayerKickEvent
import org.bukkit.event.player.PlayerQuitEvent
import org.bukkit.event.server.ServerCommandEvent
import com.doglong.skillsystem.hooker.view.component.ViewMapPart
import com.doglong.skillsystem.hooker.view.component.ViewScreen
import java.util.concurrent.CompletableFuture
import java.util.regex.Pattern

object ViewManager: Listener {


    @PacketId("setYaml")
    private val setYaml = runPacket(length = 2) { p, screen, args ->
        val part = screen[args[0]]
        if (part !is ViewAnyPart) {
            p.sendMessage("[${plugin.name}] ${args[0]} 配置节点不存在")
            return@runPacket
        }
        part.value = when(part.value) {
            is Boolean -> args[1] == "true"
            is Number -> args[1].asNumber()
            else -> args[1]
        }
        screen.syncUpdate()
    }

    private val level by lazy { object : DebugLevel() {
        override val priority: Int = 20

        init {
            this.register()
        }

        override fun debug(message: String, vararg params: Any?) {
            info("VIEW_DC >> $message", *params)
        }
    }::class.java }

    private val functions = mutableMapOf<String, ViewPacket>()

    fun setup() {
        this.register()
        this.registerPackets()
    }

    fun Any.registerPackets() {
        val clazz = this::class.java
        clazz.declaredFields.filter {
            it.isAnnotationPresent(PacketId::class.java) && it.type.equals(ViewPacket::class.java)
        }.forEach {
            val id = it.getAnnotation(PacketId::class.java)
            val packet = it.setAccessible().get(this) as ViewPacket
            functions[id.identity.uppercase()] = packet
            debug(
                level, "&6ID&a[ &f${id.identity} &a]  " +
                    "&6Length&a[ &f${packet.length} &a]  " +
                    "&6Async&a[ &f${packet.async} &a]")
        }
    }

    @EventHandler
    private fun on(e: ViewScreenCloseEvent) {
        PacketSender.sendDeleteCache(e.player, e.screen.fileName)
    }

    @EventHandler
    private fun onClose(e: CustomPacketEvent) {
        if (e.identifier == "${plugin.name}_CloseEvent") {
            if (e.data.size == 1) {
                debug(level, "&aCustomPacketEvent &6>>&f Identity[${e.identifier}]")
                ViewScreen.closeScreen(e.player, e.data[0])
            }
        }
    }

    @EventHandler
    private fun onTextChange(e: CustomPacketEvent) {
        if (e.identifier == "${plugin.name}_TextChange") {
            debug(level, "&aCustomPacketEvent &6>>&f Identity[${e.identifier}]")
            if (e.data.size == 2) {
                ViewScreen.getOpenedGui(e.player)?.let { viewScreen ->
                    viewScreen[e.data[0]]?.let {
                        if (it is ViewMapPart) {
                            val event = ViewTextChangeEvent(e.player, it.indexName, e.data[1], it, viewScreen)
                            event.callEvent()

                            PacketSender.sendRunFunction(
                                e.player,
                                viewScreen.indexName,
                                "方法.设置组件值('${it.indexName}', 'text', '${event.text}');局部变量.input='${event.text}';${ it["actions.input"]?.toString() ?: "" }",
                                false
                            )
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    private fun onText(e: ViewTextChangeEvent) {
        val regex = e.property["regular"]?.toString()
        if (regex.isNotEmpty()) {
            var text = e.text
            val pattern = Pattern.compile(regex!!)
            var count = 0
            while (text.isNotEmpty()) {
                val match = if (text.length > 1) text.substring(text.length - 1) else text
                if (!pattern.matcher(match).matches()) {
                    count++
                }
                text = text.substring(0, text.length - 1)
            }
            e.text = e.text.substring(0, e.text.length - count)
        }
    }

    @EventHandler
    private fun onQuit(e: PlayerQuitEvent) {
        ViewScreen.closeScreen(e.player, false)
        ViewScreen.showings.remove(e.player.name)
        debug(level, "PlayerQuitEvent: release '${e.player.name}' resources.")
    }

    @EventHandler
    private fun onQuit(e: PlayerKickEvent) {
        ViewScreen.closeScreen(e.player, false)
        ViewScreen.showings.remove(e.player.name)
        debug(level, "PlayerQuitEvent: release '${e.player.name}' resources.")
    }

    @EventHandler
    private fun onReload(e: PlayerCommandPreprocessEvent) {
        if (e.player.hasPermission("core.command.reload")) {
            if (e.message == "/core reload" || e.message == "/dragoncore reload") {
                debug(level, "PlayerCommandPreprocessEvent: rerender view.")
                Bukkit.getScheduler().runTaskLater(plugin, { ViewScreen.rerender() }, 10)
            }
        }
    }

    @EventHandler
    private fun onReload(e: ServerCommandEvent) {
        if (e.command == "/core reload" || e.command == "/dragoncore reload") {
            debug(level, "ServerCommandEvent: rerender view.")
            Bukkit.getScheduler().runTaskLater(plugin, { ViewScreen.rerender() }, 10)
        }
    }

    @EventHandler
    private fun onFunction(e: CustomPacketEvent) {
        if (e.identifier == plugin.name && e.data.size > 0) {
            val identity = e.data.removeAt(0)
            val function = functions[identity.uppercase()] ?: return
            if (e.data.size < function.length) {
                info("&6CustomPacketEvent >> 参数的长度不足，需要 {0} 个参数", function.length)
                return
            }
            val screen = if (function.indexName == null) {
                ViewScreen.getOpenedGui(e.player)
            }else {
                ViewScreen.getOpenedViewScreen(e.player, function.indexName)
            }
            if (screen == null) {
                if (function.indexName == null) {
                    info("&6CustomPacketEvent >> 当前没有打开任何界面")
                }else {
                    info("&6CustomPacketEvent >> 你需要打开 {0} 界面", function.indexName)
                }
                return
            }
            val event = ViewRunFunctionEvent(e.player, function.indexName, e.data, screen)
            if (!event.callEvent()) {
                return
            }
            if (function.async) {
                CompletableFuture.runAsync{
                    function.executor(event.player, event.screen, event.data)
                }
            }else {
                function.executor(event.player, event.screen, event.data)
            }
            debug(level, "CustomPacketEvent: executor '$identity' custom packet function.")
        }
    }
}