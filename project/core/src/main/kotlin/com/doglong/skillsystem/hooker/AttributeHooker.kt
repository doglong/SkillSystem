package com.doglong.skillsystem.hooker

import org.bukkit.entity.LivingEntity

interface AttributeHooker {

    fun getMana(livingEntity: LivingEntity): Int

    fun getManaRestore(livingEntity: LivingEntity): Int

    fun getCooldown(livingEntity: LivingEntity): Int

    fun getOperateData(livingEntity: LivingEntity): OperateData

    abstract class OperateData {

        abstract fun addBefore(attributes: MutableList<String>)

        abstract fun addAfter(attributes: MutableList<String>)

        abstract fun clear()

        abstract fun update()
    }


    object Default: AttributeHooker {

        override fun getMana(livingEntity: LivingEntity): Int = 100

        override fun getManaRestore(livingEntity: LivingEntity): Int = 1

        override fun getCooldown(livingEntity: LivingEntity): Int = 0

        override fun getOperateData(livingEntity: LivingEntity): OperateData {
            return EmptyOperateData()
        }

    }

    class EmptyOperateData : OperateData() {

        override fun addBefore(attributes: MutableList<String>) {
        }

        override fun addAfter(attributes: MutableList<String>) {
        }

        override fun clear() {
        }

        override fun update() {
        }

    }
}