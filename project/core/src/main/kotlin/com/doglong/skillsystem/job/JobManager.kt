package com.doglong.skillsystem.job

import com.doglong.skillsystem.common.Data
import com.doglong.skillsystem.common.OperateType
import com.doglong.skillsystem.profile.JobProfile
import com.doglong.skillsystem.profile.PlayerProfile
import org.bukkit.entity.Player

interface JobManager {

    /**
     * 全部职业的索引名
     */
    val keySet: Set<String>

    /**
     * 全部职业的集合
     */
    val jobs: Collection<Map.Entry<String, Job>>

    /**
     * 设置经验
     */
    fun setExp(player: Player, type: OperateType, exp: Long, sync: Boolean = true): Data<Any>

    /**
     * 计算达到某个等级的所需经验总值
     */
    fun calculateExpByLevel(player: Player, level: Int, job: Job): Long

    /**
     * 同步更新属性
     */
    fun updateAttributes(player: Player, profile: PlayerProfile)

    /**
     * 同步更新原版经验
     */
    fun syncMinecraftExperience(player: Player, jobProfile: JobProfile?)

    /**
     * 设置当前的职业
     */
    fun setJob(profile: PlayerProfile, job: Job?): Data<Any>

    /**
     * 根据职业索引名获取职业实例
     */
    fun getJob(instanceKey: String): Job?

}