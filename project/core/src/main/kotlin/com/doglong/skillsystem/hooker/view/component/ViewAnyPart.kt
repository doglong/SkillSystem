package com.doglong.skillsystem.hooker.view.component

import org.bukkit.configuration.file.FileConfiguration

class ViewAnyPart(
    override val indexName: String,
    var value: Any?
): ViewPart {

    override fun write(config: FileConfiguration) {
        config.set(indexName, value)
    }

}