package com.doglong.skillsystem.utils

import bukkitkotlin.plugin.plugin
import bukkitkotlin.utils.StringUtils.isNotEmpty
import org.bukkit.command.CommandSender
import org.bukkit.configuration.ConfigurationSection
import org.bukkit.entity.Player
import shark.virus.common.VirusExecutor
import shark.virus.common.VirusManager
import shark.virus.common.VirusVariable
import shark.virus.internal.VirusActive
import java.util.*

val virus: VirusManager by lazy {
    val manager = VirusActive.setup(plugin)
    manager.registerCustomFunctionByClass(VirusExtension::class.java)
    manager
}

fun String?.inlineScript(sender: Player, variable: VirusVariable.() -> Unit = {}): String {
    return virus.inlineScript(this, "{{", "}}", sender, variable)
}

fun String?.inlineScript(sender: Player, variable: VirusVariable): String {
    return virus.inlineScript(this, "{{", "}}", sender, variable)
}

fun LinkedList<VirusExecutor>?.runScript(sender: CommandSender, config: ConfigurationSection? = null, def: Any? = null, variable: VirusVariable.() -> Unit = {}): Any? {
    return this?.let {
        val vars = config?.let { c -> virus.newVarTable(c) } ?: virus.newVarTable()
        variable.invoke(vars)
        virus.runScript(sender, it, vars)
    } ?: def
}

fun String?.runScript(sender: CommandSender, config: ConfigurationSection? = null, variable: VirusVariable.() -> Unit = {}): Any? {
    return this?.let {
        val vars = config?.let { c -> virus.newVarTable(c) } ?: virus.newVarTable()
        variable.invoke(vars)
        return virus.runScript(sender, it, vars)
    }
}

fun VirusConfig.runScript(sender: CommandSender, pathname: String, variable: VirusVariable.() -> Unit = {}): Any? {
    return this.config.getString(pathname)?.let { it.runScript(sender, this.config, variable) }
}

fun String?.runScriptGetBoolean(sender: CommandSender, config: ConfigurationSection? = null, variable: VirusVariable.() -> Unit = {}): Boolean {
    return this.runScript(sender, config, variable)?.let { it.toString() == "true" } ?: false
}

fun String?.parseScript(): LinkedList<VirusExecutor>? {
    return if (this.isNotEmpty()) virus.getParsedExecutorList(this!!) else null
}