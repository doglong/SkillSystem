package com.doglong.skillsystem.hooker.view.component

import bukkitkotlin.functions.DataContainer
import bukkitkotlin.plugin.plugin
import com.doglong.skillsystem.hooker.view.ViewSectionUtil.asMap
import org.bukkit.Bukkit
import org.bukkit.configuration.ConfigurationSection
import org.bukkit.configuration.file.YamlConfiguration
import org.bukkit.entity.Player
import com.doglong.skillsystem.hooker.view.ViewSectionUtil.asValues
import com.doglong.skillsystem.hooker.view.ViewSectionUtil.check
import com.doglong.skillsystem.hooker.view.ViewSectionUtil.getMap
import com.doglong.skillsystem.event.ViewScreenCloseEvent
import com.doglong.skillsystem.event.ViewScreenOpenEvent
import eos.moe.dragoncore.network.PacketSender
import java.io.File
import java.util.concurrent.ConcurrentHashMap

class ViewScreen(
    val player: Player,
    override val indexName: String,
    values: MutableMap<String, Any?>,
): ViewContainerPart(indexName) {

    var showing = false
        private set

    val fileName: String
        get() = "Gui/${indexName}.yml"

    val dataContainer = DataContainer()

    constructor(player: Player, indexName: String) : this(player, indexName, mutableMapOf())

    constructor(player: Player, indexName: String, section: ConfigurationSection) : this(player, indexName, section.asValues())

    init {
        values.check()
        for ((k, v) in values) {
            val map = v.asMap
            if (map != null) {
                this.add(ViewMapPart(k, map))
            }else {
                this.add(ViewAnyPart(k, v))
            }
        }
    }

    fun open(isMatch: Boolean = false) {
        setOpenedScreen(player, this, false, isMatch = isMatch)
    }

    fun show() {
        setOpenedScreen(player, this, true, isMatch = false)
    }

    fun syncUpdate() {
        if (showing) {
            PacketSender.sendYaml(player, fileName, buildYaml(true))
            PacketSender.sendOpenHud(player, indexName)
        }else if(this == guis[player.name]) {
            PacketSender.sendUpdateGui(player, buildYaml(false))
        }
    }

    fun close(showing: Boolean = this.showing) {
        if (showing) {
            showings[player.name]?.remove(indexName)
            if (ViewScreenCloseEvent(player, this).callEvent()) {
                PacketSender.sendYaml(player, fileName, YamlConfiguration())
                PacketSender.sendOpenHud(player, indexName)
            }
        }else {
            closeScreen(player, true)
        }
    }

    fun buildYaml(showing: Boolean): YamlConfiguration {
        val yaml = YamlConfiguration()
        this.write(yaml)
        if (!showing) {
            val map = yaml.get("Functions").asMap
            if (map != null) {
                val closeFunc = map.getOrDefault("close", "")
                map["close"] = "方法.发包('${plugin.name}_CloseEvent', '${indexName}');${closeFunc}"
                yaml.set("Functions", map)
            }
        }
        return yaml
    }

    companion object {

        internal val guis = ConcurrentHashMap<String, ViewScreen>()
        internal val showings = ConcurrentHashMap<String, MutableMap<String, ViewScreen>>()

        fun rerender() {
            Bukkit.getOnlinePlayers().forEach {
                guis[it.name]?.open()
                showings[it.name]?.let { map -> for ((_, v) in map) { v.show()
                } }
            }
        }

        fun getOpenedGui(player: Player): ViewScreen? {
            return guis[player.name]
        }

        fun getOpenedHud(player: Player, indexName: String): ViewScreen? {
            return showings[player.name]?.get(indexName)
        }

        fun getOpenedViewScreen(player: Player, indexName: String): ViewScreen? {
           return getOpenedGui(player)?.let {
                if (it.indexName == indexName) {
                    it
                }else null
            } ?: getOpenedHud(player, indexName)
        }

        internal fun setOpenedScreen(player: Player, screen: ViewScreen, showing: Boolean, isMatch: Boolean) {
            if (showing) {
                val map = showings.computeIfAbsent(player.name) { mutableMapOf() }
                // 如果已经是以界面的方式打开，则需要关闭这个界面
                if (guis.containsKey(player.name) && guis[player.name]!!.indexName == screen.indexName) {
                    screen.close(false)
                }
                screen.showing = true
                map[screen.indexName] = screen
                PacketSender.sendYaml(player, screen.fileName, screen.buildYaml(true))
                PacketSender.sendOpenHud(player, screen.indexName)
                ViewScreenOpenEvent(player, screen).callEvent()
            }else {
                // 如果这个图形已经是以HUD的方式打开
                if (screen.showing) {
                    screen.close()
                }

                // 如果这个玩家已打开别的界面，则关闭那个界面
                closeScreen(player, false)

                screen.showing = false
                guis[player.name] = screen
                PacketSender.sendYaml(player, screen.fileName, screen.buildYaml(false))
                if (!isMatch) {
                    PacketSender.sendOpenGui(player, screen.indexName)
                }
                ViewScreenOpenEvent(player, screen).callEvent()
            }
        }

        internal fun closeScreen(player: Player, force: Boolean): ViewScreen? {
            val remove = guis.remove(player.name)
            if (remove != null) {
                ViewScreenCloseEvent(player, remove).callEvent()
            }
            if (force) {
                player.closeInventory()
                remove?.let { PacketSender.sendDeleteCache(player, it.fileName) }
            }
            return remove
        }

        internal fun closeScreen(player: Player, guiName: String) {
            val remove = guis[player.name]
            if (remove?.indexName == guiName) {
                guis.remove(player.name)
                ViewScreenCloseEvent(player, remove).callEvent()
            }
        }
    }
}