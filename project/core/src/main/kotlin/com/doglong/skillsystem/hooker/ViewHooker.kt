package com.doglong.skillsystem.hooker

import com.doglong.skillsystem.profile.PlayerProfile
import org.bukkit.configuration.ConfigurationSection
import org.bukkit.entity.Player

interface ViewHooker {

    fun loadGui(config: ConfigurationSection)

    fun loadBar(config: ConfigurationSection)

    fun openBar(player: Player, profile: PlayerProfile)

    fun openGui(player: Player, profile: PlayerProfile)

    fun updateBar(player: Player, profile: PlayerProfile)

    fun updateGui(player: Player, profile: PlayerProfile)

    fun renderCooldown(player: Player, profile: PlayerProfile)

    fun registerKey(key: String)

    fun unregisterKey(key: String)

    object Default: ViewHooker {

        override fun loadGui(config: ConfigurationSection) {
        }

        override fun loadBar(config: ConfigurationSection) {
        }

        override fun openBar(player: Player, profile: PlayerProfile) {
        }

        override fun openGui(player: Player, profile: PlayerProfile) {
        }

        override fun updateBar(player: Player, profile: PlayerProfile) {
        }

        override fun updateGui(player: Player, profile: PlayerProfile) {
        }

        override fun renderCooldown(player: Player, profile: PlayerProfile) {
        }

        override fun registerKey(key: String) {
        }

        override fun unregisterKey(key: String) {
        }

    }
}