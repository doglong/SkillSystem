package com.doglong.skillsystem.event;

import bukkitkotlin.event.BaseEvent;
import com.doglong.skillsystem.profile.PlayerProfile;
import com.doglong.skillsystem.profile.SkillProfile;
import com.doglong.skillsystem.skill.Skill;
import org.bukkit.entity.Entity;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

import java.util.Set;

public final class SkillEvents {

    private SkillEvents() {
    }

    public static class Damage extends BaseEvent implements Cancellable {
        private static final HandlerList handlers = new HandlerList();
        private double damage;
        private boolean cancel;
        private final Entity attacker;
        private final Entity defender;

        private final String damageAttribute;
        private final Set<String> attributeCalculateList;

        public Damage(double damage, Entity attacker, Entity defender, String damageAttribute, Set<String> attributeCalculateList) {
            this.damage = damage;
            this.attacker = attacker;
            this.defender = defender;
            this.damageAttribute = damageAttribute;
            this.attributeCalculateList = attributeCalculateList;
        }

        @Override
        public HandlerList getHandlers() {
            return handlers;
        }

        public static HandlerList getHandlerList() {
            return handlers;
        }

        @Override
        public boolean isCancelled() {
            return cancel;
        }

        @Override
        public void setCancelled(boolean cancel) {
            this.cancel = cancel;
        }

        public double getDamage() {
            return damage;
        }

        public void setDamage(double damage) {
            this.damage = damage;
        }

        public Entity getAttacker() {
            return attacker;
        }

        public Entity getDefender() {
            return defender;
        }

        public String getDamageAttribute() {
            return damageAttribute;
        }

        public Set<String> getAttributeCalculateList() {
            return attributeCalculateList;
        }

    }

    /**
     * 技能等级改变事件
     * @Author 鲨鱼酱
     * @Date 2023/4/13 5:29
     */
    public static class LevelChange extends BaseEvent implements Cancellable {

        private static final HandlerList handlers = new HandlerList();
        private boolean cancel;
        private String cancelMessage;
        private final PlayerProfile playerProfile;
        private final SkillProfile skillProfile;
        private final int amount;
        private final int oleLevel;

        public LevelChange(PlayerProfile playerProfile, SkillProfile skillProfile, int amount, int oleLevel) {
            this.playerProfile = playerProfile;
            this.skillProfile = skillProfile;
            this.amount = amount;
            this.oleLevel = oleLevel;
        }

        @Override
        public boolean isCancelled() {
            return this.cancel;
        }

        @Override
        public void setCancelled(boolean cancel) {
            this.cancel = cancel;
        }

        @Override
        public HandlerList getHandlers() {
            return handlers;
        }

        public static HandlerList getHandlerList() {
            return handlers;
        }

        public PlayerProfile getPlayerProfile() {
            return playerProfile;
        }

        public SkillProfile getSkillProfile() {
            return skillProfile;
        }

        public int getAmount() {
            return amount;
        }

        public int getOleLevel() {
            return oleLevel;
        }

        public String getCancelMessage() {
            return cancelMessage;
        }

        public void setCancelMessage(String cancelMessage) {
            this.cancelMessage = cancelMessage;
        }
    }


    public static class AddSkill extends BaseEvent implements Cancellable {

        private static final HandlerList handlers = new HandlerList();
        private boolean cancel;
        private String cancelMessage;
        private final PlayerProfile playerProfile;
        private final Skill skill;

        public AddSkill(PlayerProfile playerProfile, Skill skill) {
            this.playerProfile = playerProfile;
            this.skill = skill;
        }

        @Override
        public boolean isCancelled() {
            return this.cancel;
        }

        @Override
        public void setCancelled(boolean cancel) {
            this.cancel = cancel;
        }

        @Override
        public HandlerList getHandlers() {
            return handlers;
        }

        public static HandlerList getHandlerList() {
            return handlers;
        }

        public PlayerProfile getPlayerProfile() {
            return playerProfile;
        }

        public Skill getSkill() {
            return skill;
        }

        public String getCancelMessage() {
            return cancelMessage;
        }

        public void setCancelMessage(String cancelMessage) {
            this.cancelMessage = cancelMessage;
        }
    }


    public static final class Cast {

        private Cast() {
        }

        public static class Before extends BaseEvent implements Cancellable {

            private static final HandlerList handlers = new HandlerList();
            private boolean cancel;
            private final PlayerProfile playerProfile;
            private int mana;
            private int level;
            private int cooldown;
            private final Skill skill;

            public Before(PlayerProfile playerProfile, int mana, int level, int cooldown, Skill skill) {
                this.playerProfile = playerProfile;
                this.mana = mana;
                this.level = level;
                this.cooldown = cooldown;
                this.skill = skill;
            }

            @Override
            public HandlerList getHandlers() {
                return handlers;
            }

            public static HandlerList getHandlerList() {
                return handlers;
            }

            @Override
            public boolean isCancelled() {
                return cancel;
            }

            @Override
            public void setCancelled(boolean cancel) {
                this.cancel = cancel;
            }

            public int getMana() {
                return mana;
            }

            public void setMana(int mana) {
                this.mana = mana;
            }

            public int getLevel() {
                return level;
            }

            public void setLevel(int level) {
                this.level = level;
            }

            public int getCooldown() {
                return cooldown;
            }

            public void setCooldown(int cooldown) {
                this.cooldown = cooldown;
            }

            public PlayerProfile getPlayerProfile() {
                return playerProfile;
            }

            public Skill getSkill() {
                return skill;
            }
        }

        public static class After extends BaseEvent {

            private static final HandlerList handlers = new HandlerList();
            private final int level;
            private final Skill skill;
            private final PlayerProfile playerProfile;

            public After(PlayerProfile playerProfile, int level, Skill skill) {
                this.playerProfile = playerProfile;
                this.level = level;
                this.skill = skill;
            }

            @Override
            public HandlerList getHandlers() {
                return handlers;
            }

            public static HandlerList getHandlerList() {
                return handlers;
            }

            public int getLevel() {
                return level;
            }

            public Skill getSkill() {
                return skill;
            }

            public PlayerProfile getPlayerProfile() {
                return playerProfile;
            }
        }
    }
}
