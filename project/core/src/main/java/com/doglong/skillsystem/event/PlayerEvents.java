package com.doglong.skillsystem.event;

import bukkitkotlin.event.BaseEvent;
import com.doglong.skillsystem.profile.PlayerProfile;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;

public final class PlayerEvents {

    private PlayerEvents() {
    }

    public static class Release extends BaseEvent {

        private static final HandlerList handlers = new HandlerList();
        private final PlayerProfile profile;

        public Release(PlayerProfile profile) {
            this.profile = profile;
        }

        @Override
        public HandlerList getHandlers() {
            return handlers;
        }

        public static HandlerList getHandlerList() {
            return handlers;
        }

        public PlayerProfile getProfile() {
            return profile;
        }

        public Player getPlayer() {
            return profile.getPlayer();
        }
    }

    public static class Loaded extends BaseEvent {

        private static final HandlerList handlers = new HandlerList();
        private final PlayerProfile playerProfile;

        public Loaded(PlayerProfile playerProfile) {
            this.playerProfile = playerProfile;
        }

        @Override
        public HandlerList getHandlers() {
            return handlers;
        }

        public static HandlerList getHandlerList() {
            return handlers;
        }

        public PlayerProfile getPlayerProfile() {
            return playerProfile;
        }

        public Player getPlayer() {
            return playerProfile.getPlayer();
        }
    }

    public static class Tag extends BaseEvent {

        private static final HandlerList handlers = new HandlerList();
        private final PlayerProfile playerProfile;
        private final String key;
        private final String value;

        public Tag(PlayerProfile playerProfile, String key, String value) {
            this.playerProfile = playerProfile;
            this.key = key;
            this.value = value;
        }

        @Override
        public HandlerList getHandlers() {
            return handlers;
        }

        public static HandlerList getHandlerList() {
            return handlers;
        }

        public String getKey() {
            return key;
        }

        public String getValue() {
            return value;
        }

        public PlayerProfile getPlayerProfile() {
            return playerProfile;
        }

        public Player getPlayer() {
            return playerProfile.getPlayer();
        }

    }

}
