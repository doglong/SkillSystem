package com.doglong.skillsystem.event;

import bukkitkotlin.event.BaseEvent;
import com.doglong.skillsystem.hooker.view.component.ViewScreen;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

import java.util.List;

public class ViewRunFunctionEvent extends BaseEvent implements Cancellable {

    private static final HandlerList handlers = new HandlerList();
    private final Player player;
    private final String function;
    private final List<String> data;
    private final ViewScreen screen;
    private boolean cancel;

    public ViewRunFunctionEvent(Player player, String function, List<String> data, ViewScreen screen) {
        this.player = player;
        this.function = function;
        this.data = data;
        this.screen = screen;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public Player getPlayer() {
        return player;
    }

    public ViewScreen getScreen() {
        return screen;
    }

    public String getFunction() {
        return function;
    }

    public List<String> getData() {
        return data;
    }

    @Override
    public boolean isCancelled() {
        return this.cancel;
    }

    @Override
    public void setCancelled(boolean cancel) {
        this.cancel = cancel;
    }
}
