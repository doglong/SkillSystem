package com.doglong.skillsystem.event;

import bukkitkotlin.event.BaseEvent;
import com.doglong.skillsystem.hooker.view.component.ViewMapPart;
import com.doglong.skillsystem.hooker.view.component.ViewScreen;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;

public class ViewTextChangeEvent extends BaseEvent {

    private static final HandlerList handlers = new HandlerList();
    private final Player player;
    private final String indexName;
    private String text;
    private final ViewMapPart property;
    private final ViewScreen screen;

    public ViewTextChangeEvent(Player player, String indexName, String text, ViewMapPart property, ViewScreen screen) {
        this.player = player;
        this.indexName = indexName;
        this.text = text;
        this.property = property;
        this.screen = screen;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public Player getPlayer() {
        return player;
    }

    public ViewScreen getScreen() {
        return screen;
    }

    public String getIndexName() {
        return indexName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ViewMapPart getProperty() {
        return property;
    }
}
