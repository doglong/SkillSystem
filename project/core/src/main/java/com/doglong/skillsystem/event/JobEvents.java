package com.doglong.skillsystem.event;

import bukkitkotlin.event.BaseEvent;
import com.doglong.skillsystem.common.OperateType;
import com.doglong.skillsystem.profile.JobProfile;
import com.doglong.skillsystem.profile.PlayerProfile;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

public final class JobEvents {

    private JobEvents() {
    }

    public static class Change {

        private Change() {
        }

        public static class Per extends BaseEvent implements Cancellable {

            private static final HandlerList handlers = new HandlerList();
            private final PlayerProfile playerProfile;
            private final JobProfile originalJob;
            private final JobProfile jobProfile;
            private boolean cancel;
            private String cancelMessage;

            public Per(PlayerProfile playerProfile, JobProfile originalJob, JobProfile jobProfile) {
                this.playerProfile = playerProfile;
                this.originalJob = originalJob;
                this.jobProfile = jobProfile;
            }

            @Override
            public HandlerList getHandlers() {
                return handlers;
            }

            public static HandlerList getHandlerList() {
                return handlers;
            }

            @Override
            public boolean isCancelled() {
                return cancel;
            }

            @Override
            public void setCancelled(boolean cancel) {
                this.cancel = cancel;
            }

            public String getCancelMessage() {
                return cancelMessage;
            }

            public void setCancelMessage(String cancelMessage) {
                this.cancelMessage = cancelMessage;
            }

            public Player getPlayer() {
                return playerProfile.getPlayer();
            }

            public PlayerProfile getPlayerProfile() {
                return playerProfile;
            }

            public JobProfile getOriginalJob() {
                return originalJob;
            }

            public JobProfile getJobProfile() {
                return jobProfile;
            }
        }


        public static class Post extends BaseEvent {

            private static final HandlerList handlers = new HandlerList();
            private final PlayerProfile playerProfile;
            private final JobProfile jobProfile;

            public Post(PlayerProfile playerProfile, JobProfile jobProfile) {
                this.playerProfile = playerProfile;
                this.jobProfile = jobProfile;
            }

            @Override
            public HandlerList getHandlers() {
                return handlers;
            }

            public static HandlerList getHandlerList() {
                return handlers;
            }

            public Player getPlayer() {
                return playerProfile.getPlayer();
            }

            public PlayerProfile getPlayerProfile() {
                return playerProfile;
            }

            public JobProfile getJobProfile() {
                return jobProfile;
            }
        }
    }


    public static class ExpChange extends BaseEvent implements Cancellable {

        private static final HandlerList handlers = new HandlerList();
        private final Player player;
        private final long original;
        private long amount;
        private OperateType type;
        private boolean cancel;

        public ExpChange(Player who, long original, long amount, OperateType type) {
            this.player = who;
            this.original = original;
            this.amount = amount;
            this.type = type;
        }

        @Override
        public HandlerList getHandlers() {
            return handlers;
        }

        public static HandlerList getHandlerList() {
            return handlers;
        }

        @Override
        public boolean isCancelled() {
            return cancel;
        }

        @Override
        public void setCancelled(boolean cancel) {
            this.cancel = cancel;
        }

        public Player getPlayer() {
            return player;
        }

        public long getOriginal() {
            return original;
        }

        public long getAmount() {
            return amount;
        }

        public void setAmount(long amount) {
            this.amount = amount;
        }

        public OperateType getType() {
            return type;
        }

        public void setType(OperateType type) {
            this.type = type;
        }
    }

    public static class LevelChange extends BaseEvent {

        private static final HandlerList handlers = new HandlerList();
        private final Player player;
        private final int oldLevel;
        private final int amount;

        public LevelChange(Player player, int oldLevel, int amount) {
            this.player = player;
            this.oldLevel = oldLevel;
            this.amount = amount;
        }

        @Override
        public HandlerList getHandlers() {
            return handlers;
        }

        public static HandlerList getHandlerList() {
            return handlers;
        }

        public Player getPlayer() {
            return player;
        }

        public int getOldLevel() {
            return oldLevel;
        }

        public int getAmount() {
            return amount;
        }
    }

}
